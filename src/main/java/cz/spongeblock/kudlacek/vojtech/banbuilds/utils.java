package cz.spongeblock.kudlacek.vojtech.banbuilds;

import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.ArrayList;

public final class utils {
    public static String[][] twoDArrayListTo2dArray(ArrayList<ArrayList<String>> twoDArrayList) {
        String[][] twoDArray = new String[twoDArrayList.size()][];
        for (int i = 0; i < twoDArrayList.size(); i++) {
            String[] tmp = new String[twoDArrayList.get(i).size()];
            for (int j = 0; j < twoDArrayList.get(i).size(); j++) {
                tmp[j] = twoDArrayList.get(i).get(j);
            }
            twoDArray[i] = tmp;
        }
        return twoDArray;
    }

    public static int[] convertToAbsCords(int x, int z, World world) {
        int[] resultCords = new int[2];
        if (world == null)
            world = Bukkit.getWorld("world");
        int worldSize = (int) world.getWorldBorder().getSize()/2;
        resultCords[0] = x + worldSize;
        resultCords[1] = z + worldSize;
        return resultCords;
    }

    public static int[] convertFromAbsCords(int x, int z, World world) {
        int[] resultCords = new int[2];
        if (world == null)
            world = Bukkit.getWorld("world");
        int worldSize = (int) world.getWorldBorder().getSize()/2;
        resultCords[0] = x - worldSize;
        resultCords[1] = z - worldSize;
        return resultCords;
    }
}
