package cz.spongeblock.kudlacek.vojtech.banbuilds;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public final class BanBuilds extends JavaPlugin {
    static File dataFolder;
    private BuildBreakeListener listener = new BuildBreakeListener();
    private ArrayList<String> coords;
    private ArrayList<String> names;

    @Override
    public void onEnable() {
        if (!new File(dataFolder, "patterns.yaml").isFile()) {
            try {
                generatePatterns();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        dataFolder = getDataFolder();
        listener.patterns = new HashMap<>();
        coords = new ArrayList<>();
        names = new ArrayList<>();
        loadPatterns();
        getServer().getPluginManager().registerEvents(this.listener, this);
    }

    private void generatePatterns() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(dataFolder,"patterns.yaml")));
        bw.write("nameOfPattern: [['1','0','0'],\n" +
                "['0','0','1']]");
        bw.flush();
        bw.close();
    }

    private void loadPatterns() {
        Yaml yaml = new Yaml();
        try {
            Iterable it = yaml.loadAll(new FileInputStream(new File(dataFolder, "patterns.yaml")));
            HashMap<String, ArrayList<ArrayList<String>>> rawData;
            rawData = (HashMap<String, ArrayList<ArrayList<String>>>) it.iterator().next();
            for (String name : rawData.keySet()) {
                listener.patterns.put(name, new Pattern(utils.twoDArrayListTo2dArray(rawData.get(name))));
            }
        } catch (ClassCastException e) {
            System.err.println("Bad pattern format!");
            // e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        System.out.println("BBomer");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        loadData();
        if (command.getName().equalsIgnoreCase("builds") && sender.isOp()) {
            for (int i = 0; i < names.size(); i++) {
                sender.sendMessage(coords.get(i) + " - " + names.get(i));
            }

            return true;
        } else if (command.getName().equalsIgnoreCase("delbuild") && sender.isOp()) {
            if (args.length != 2) {
                sender.sendMessage(ChatColor.RED + "Bad arguments !");
                return false;
            } else {
                for (int i = 0; i < coords.size(); i++) {
                    String[] rawCoords = coords.get(i).replace(" ","").replace("(", "").replace(")", "").split(",");
                    if (rawCoords[0].equals(args[0]) && rawCoords[2].equals(args[1])) {
                        coords.remove(i);
                        names.remove(i);
                    }
                }
                File file = new File(dataFolder,"data.bb");
                file.delete();
                try {
                    file.createNewFile();
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
                    for (int i = 0; i < coords.size(); i++) {
                        bufferedWriter.write(coords.get(i) + " - " + names.get(i));
                        bufferedWriter.newLine();
                        bufferedWriter.flush();
                    }
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                sender.sendMessage(ChatColor.GREEN + "Success");
                return true;
            }
        }
        return false;
    }

    private void loadData() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(dataFolder, "data.bb")));
            coords.clear();
            names.clear();
            String line;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split(" - ");
                coords.add(lines[0]);
                names.add(lines[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
