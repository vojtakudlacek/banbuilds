package cz.spongeblock.kudlacek.vojtech.banbuilds;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class BuildBreakeListener implements Listener {

    public HashMap<String, Pattern> patterns;

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) throws IOException {
        Location loc = event.getBlock().getLocation();
        String name = isPartOfStructure(loc);
        if (name != "") {
            writeToLog(loc, name);
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) throws IOException {
        Location loc = event.getBlock().getLocation();
        String name = isPartOfStructure(loc);
        if (name != "") {
            writeToLog(loc, name);
        }
    }

    private void writeToLog(Location loc, String detectedType) throws IOException {
        File dataFolder = BanBuilds.dataFolder;
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(dataFolder, "data.bb"), true));
        bw.write("(" + loc.getBlockX() + "," + loc.getBlockY() + ", " + loc.getBlockZ() + ") - " + detectedType);
        bw.newLine();
        bw.flush();
        bw.close();
    }

    private String isPartOfStructure(Location loc) {
        boolean match = true;

        for (String key : patterns.keySet()) { //horizontal
            Pattern pattern = patterns.get(key);
            Location A = new Location(loc.getWorld(), loc.getBlockX() - (pattern.getPatern().length - 1), loc.getY(), loc.getZ() - (pattern.getPatern()[0].length - 1));
            Location S = loc.clone();
            match = compareArea(pattern, loc, A, S);
            for (int i = 0; i < 3; i++) {
                pattern = new Pattern(pattern.getRotatedPattern(i));
                A = new Location(loc.getWorld(), loc.getBlockX() - (pattern.getPatern().length - 1), loc.getY(), loc.getZ() - (pattern.getPatern()[0].length - 1));
                match = match || compareArea(pattern, loc, A, S);
                if (match)
                    return key;
            }

            pattern = patterns.get(key); // veritcal
            A = new Location(loc.getWorld(), loc.getBlockX() - (pattern.getPatern().length - 1), loc.getY() - (pattern.getPatern()[0].length - 1), loc.getZ());
            S = loc.clone();
            match = compareArea(pattern, loc, A, S);
            for (int i = 0; i < 3; i++) {
                pattern = new Pattern(pattern.getRotatedPattern(i));
                A = new Location(loc.getWorld(), loc.getBlockX() - (pattern.getPatern().length - 1), loc.getY() - (pattern.getPatern()[0].length - 1), loc.getZ());
                match = match || compareAreaY(pattern, loc, A, S);
                if (match)
                    return key;
            }
        }

        return "";
    }

    private boolean compareAreaY(Pattern pattern, Location blockToCompareLocation, Location StartLocation, Location EndLocation) {
        boolean result = true;
        int z[] = utils.convertToAbsCords(StartLocation.getBlockY(), EndLocation.getBlockY(), StartLocation.getWorld());
        int x[] = utils.convertToAbsCords(StartLocation.getBlockX(), EndLocation.getBlockX(), StartLocation.getWorld());

        for (int i = z[0]; i <= z[1]; i++) { // now only works in +,+ quadrant
            for (int j = x[0]; j <= x[1]; j++) {
                int[] coords = utils.convertFromAbsCords(i, j, StartLocation.getWorld());
                result = compareMatricesZ(coords[0], coords[1], blockToCompareLocation, pattern, result);
                if (result) {
                    return true;
                } else
                    result = true;
            }
        }
        return false;
    }
    private boolean compareArea(Pattern pattern, Location blockToCompareLocation, Location StartLocation, Location EndLocation) {
        boolean result = true;
        int z[] = utils.convertToAbsCords(StartLocation.getBlockZ(), EndLocation.getBlockZ(), StartLocation.getWorld());
        int x[] = utils.convertToAbsCords(StartLocation.getBlockX(), EndLocation.getBlockX(), StartLocation.getWorld());

        for (int i = z[0]; i <= z[1]; i++) { // now only works in +,+ quadrant
            for (int j = x[0]; j <= x[1]; j++) {
                int[] coords = utils.convertFromAbsCords(i, j, StartLocation.getWorld());
                result = compareMatrices(coords[0], coords[1], blockToCompareLocation, pattern, result);
                if (result) {
                    return true;
                } else
                    result = true;
            }
        }
        return false;
    }

    private boolean compareMatrices(int i, int j, Location blockToCompareLocation, Pattern pattern, boolean result) {
        for (int k = 0; k < pattern.getPatern().length; k++) {
            for (int l = 0; l < pattern.getPatern()[k].length; l++) {
                double blockPositionX = j + k;
                double blockPositionZ = i + l; //or switch k and l
                if (!(new Location(blockToCompareLocation.getWorld(), blockPositionX, blockToCompareLocation.getY(), blockPositionZ).getBlock().isEmpty() && pattern.getPatern()[k][l].equals("0")
                        || !new Location(blockToCompareLocation.getWorld(), blockPositionX, blockToCompareLocation.getY(), blockPositionZ).getBlock().isEmpty() && pattern.getPatern()[k][l].equals("1"))) {
                    result = false;
                }
            }
        }
        return result;
    }

    private boolean compareMatricesZ(int i, int j, Location blockToCompareLocation, Pattern pattern, boolean result) {
        for (int k = 0; k < pattern.getPatern().length; k++) {
            for (int l = 0; l < pattern.getPatern()[k].length; l++) {
                double blockPositionX = j + k;
                double blockPositionZ = i + l; //or switch k and l
                if (!(new Location(blockToCompareLocation.getWorld(), blockPositionX, blockPositionZ, blockToCompareLocation.getY()).getBlock().isEmpty() && pattern.getPatern()[k][l].equals("0")
                        || !new Location(blockToCompareLocation.getWorld(), blockPositionX, blockPositionZ, blockToCompareLocation.getY()).getBlock().isEmpty() && pattern.getPatern()[k][l].equals("1"))) {
                    result = false;
                }
            }
        }
        return result;
    }
}
