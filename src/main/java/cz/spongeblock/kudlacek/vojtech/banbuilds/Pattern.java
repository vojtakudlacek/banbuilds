package cz.spongeblock.kudlacek.vojtech.banbuilds;

import java.util.Arrays;

public class Pattern {
    String[][] patern;

    public Pattern(String[][] patern) {
        this.patern = patern;
    }

    public String[][] getPatern() {
        return patern;
    }

    @Override
    public String toString() {
        return "Pattern{" +
                "patern=" + Arrays.deepToString(patern) +
                '}';
    }

    public String[][] getSizedPattern(Byte scale) {
        String[][] scaledPatern = new String[scale * patern.length][scale * patern[0].length];
        for (int i = 0; i < patern.length; i++) {
            for (int j = 0; j < patern[i].length; j++) {
                for (int k = 0; k < scale; k++) {
                    scaledPatern[i][j + k] = patern[i][j];
                }
            }
            for (int j = 0; j < scale; j++) {
                scaledPatern[i * scale + j] = scaledPatern[i];
            }
        }
        return scaledPatern;
    }

    public String[][] getRotatedPattern(int howMouch) {
        String[][] rotated = new String[patern[0].length][patern.length];
        for (int i = 0; i < patern.length; i++) {
            for (int j = 0; j < patern[i].length; j++) {
                rotated[i][j] = patern[patern[i].length-1-j][i];
            }
        }
        if (howMouch != 0) {
            this.patern = rotated;
            getRotatedPattern(howMouch - 1);
        }
        return rotated;
    }
}
